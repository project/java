<?php

/**
 * @file
 * This is the tempate for the JNLP file to output.
 */

// We print the XML header as otherwise the PHP parser might choke on "<?xml".
print '<?xml version="1.0" encoding="UTF-8"?>';

$current_url = check_plain(url(current_path(), array('absolute' => TRUE)));
?>
<jnlp spec="6.0+" href="<?php print $current_url; ?>" codebase="<?php print substr($current_url, 0, strrpos($current_url, '/')); ?>">
  <information>
    <title><?php print check_plain($title); ?></title>
    <vendor><?php print check_plain($vendor); ?></vendor>
    <?php if ($offline) print '<offline-allowed/>'; ?>
  </information>
  <security>
    <all-permissions/>
  </security>
  <resources>
    <java version="<?php print check_plain($java_version); ?>"/>
    <?php
    $first = TRUE;
    foreach(explode("\n", $jars) as $jar) {
      $jar = trim($jar);
      if (empty($jar)) {
        continue;
      }
      $jar = check_plain($jar);
      print "<jar href='$jar' ";
      if ($first) {
        $first = FALSE;
        print 'main="true"';
      }
      print "/>\n";
    }

    if ($pack200) {
      print '<property name="jnlp.packEnabled" value="true"/>';
    }
    ?>
  </resources>
  <application-desc main-class="<?php print check_plain($mainclass); ?>">
    <?php 
    foreach(explode("\n", $arguments) as $argument) {
      $argument = trim($argument);
      if (empty($argument)) {
        continue;
      }
      $argument = token_replace($argument);
      $argument = check_plain($argument);
      print "<argument>$argument</argument>\n";
    }
    ?>
  </application-desc>
</jnlp>
